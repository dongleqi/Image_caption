from init import *

def get_cnn_model():
    # CNN模型
    base_model = efficientnet.EfficientNetB0(
        input_shape=(*IMAGE_SIZE, 3), include_top=False, weights="imagenet",
    )
    # 冻住特征提取层
    base_model.trainable = False
    base_model_out = base_model.output
    # 我们要修改输出层，(n,100,1280)
    base_model_out = layers.Reshape((-1, base_model_out.shape[-1]))(base_model_out)
    cnn_model = keras.models.Model(base_model.input, base_model_out)
    return cnn_model

def get_vgg19_model():
    conv_base = VGG19(weights='imagenet', include_top=False,input_shape=(*IMAGE_SIZE, 3))
    base_model = keras.Sequential()
    base_model.add(conv_base)
    base_model.trainable = False
    base_model.add(keras.layers.GlobalAveragePooling2D())
    base_model.add(keras.layers.Dense(512, activation='relu'))
    base_model.add(Dense(128, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001)))
    base_model.add(layers.Dropout(0.3))
    base_model.add(Dense(128, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001)))
    base_model.add(layers.Dropout(0.2))
    base_model.add(Dense(128, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001)))
    base_model.add(layers.Dropout(0.1))
    base_model_out = base_model.output
    # 我们要修改输出层，(n,100,1280)
    base_model_out = layers.Reshape((-1, base_model_out.shape[-1]))(base_model_out)
    cnn_model = keras.models.Model(base_model.input, base_model_out)
    return cnn_model
print(get_vgg19_model().summary())

def get_mobilenet_model():
    conv_base = keras.applications.mobilenet_v2.MobileNetV2(weights='imagenet', include_top=False,input_shape=(*IMAGE_SIZE, 3))
    base_model = keras.Sequential()
    base_model.add(conv_base)
    base_model.trainable = False
    base_model.add(Flatten(input_shape=conv_base.output_shape[1:]))
    base_model.add(Dense(128, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001)))
    base_model.add(Dense(128, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001)))
    base_model.add(Dense(128, activation='tanh', kernel_regularizer=keras.regularizers.l2(0.001)))
    base_model_out = base_model.output
    # 我们要修改输出层，(n,100,1280)
    base_model_out = layers.Reshape((-1, base_model_out.shape[-1]))(base_model_out)
    cnn_model = keras.models.Model(base_model.input, base_model_out)
    return cnn_model

def get_inceptation_model():
    conv_base = InceptionV3(weights='imagenet', include_top=False,input_shape=(*IMAGE_SIZE, 3))
    base_model = keras.Sequential()
    base_model.add(conv_base)

    base_model_out = base_model.output
    # 我们要修改输出层，(n,100,1280)
    base_model_out = layers.Reshape((-1, base_model_out.shape[-1]))(base_model_out)
    cnn_model = keras.models.Model(base_model.input, base_model_out)
    return cnn_model


def get_EFB4_model():
    base_model = efficientnet.EfficientNetB4(
        input_shape=(*IMAGE_SIZE, 3), include_top=False, weights="imagenet",
    )
    # 冻住特征提取层
    base_model.trainable = False
    base_model_out = base_model.output
    # 我们要修改输出层，(n,100,1280)
    base_model_out = layers.Reshape((-1, base_model_out.shape[-1]))(base_model_out)
    cnn_model = keras.models.Model(base_model.input, base_model_out)
    return cnn_model