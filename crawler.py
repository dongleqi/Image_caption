from urllib import request
import os
import re #正则表达式库
from urllib.parse import quote

keyword =['动物','植物','体育','玩耍','星空','雪','水果','海洋','山脉','聚会','美食']

keywords = [quote(i) for i in keyword]
print(keywords)

#输入类别名称,子类别名称，文件名，输出图片路径
def get_path(classname,subclassname,filename):
    #获取当前工作路径
    cwd = os.getcwd()
    #获取图像的保存目录
    dir_path = cwd+'/vcg_test/' + classname +'/'+ subclassname
    #目录是否存在,不存在则创建目录
    if os.path.exists(dir_path):
        pass
    else:
        os.makedirs(dir_path)
    #获取图像的绝对路径
    file_path = dir_path +'/'+ filename
    return file_path


gender_file_path = ['male', 'female']  # 对于人的分类检索可以按性别筛选
all_page = 200  # 想要下载的总页数,其中每页与检索相同,为100张
for class_index, phrase in enumerate(keywords):
    sum_all_num = 0
    if class_index >= 0:
        for gender_index, gender in enumerate(gender_file_path):
            if gender_index >= 0:  # 从性别某类断开则选择该类为起始点
                for page in range(1, all_page + 1):
                    num_in_page = 1
                    # 获得url链接,这里额外增加了筛选条件:图片中仅有一个人
                    url = 'https://www.vcg.com/creative/search?phrase=' + keywords[
                        class_index]+ '&page=' + str(
                        page)
                    header = {
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36',
                        'Cookie':'acw_tc=276077c716896031850636405ecec3871fb19b64bec971132395048d53dadc; sajssdk_2015_cross_new_user=1; clientIp=106.47.203.68; uuid=4340e18f-8580-4033-993a-98a314c912ba; FZ_STROAGE.vcg.com=eyJTRUVTSU9OSUQiOiI5OTNjMjJhYjk2MWJmNmNiIiwiU0VFU0lPTkRBVEUiOjE2ODk2MDQxMzM4MTB9; Hm_lvt_5fd2e010217c332a79f6f3c527df12e9=1689603187,1689604135; ARK_ID=undefined; fingerprint=2dda1dc39f46fd642f48b5bc45fc3e85; _fp_=eyJpcCI6IjEwNi40Ny4yMDMuNjgiLCJmcCI6IjJkZGExZGMzOWY0NmZkNjQyZjQ4YjViYzQ1ZmMzZTg1IiwiaHMiOiIkMmEkMDgkUHB1SE5YbmlMdllzamhvTHozYURrLk1LRndhQTlYUjhXcmxxb29rTktHd2hGYU40WkEwUksifQ%3D%3D; api_token=ST-709-a20ca6d3d4ef2aa9809608ae5aa9b5896; Hm_lpvt_5fd2e010217c332a79f6f3c527df12e9=1689604197; name=15604780612; ssxmod_itna2=Yq+xgDcDnDyjDteGHfox7uuCx0W=NHobeDuiDnKSioDseCQCDLWtQQk9ltqn+btelShY5wFDM127dC3OIeYIFQlZGox4/Gh1ON4avzuyGpGkf6yGLv5L9lpHzxw==xCvTKtt=+7oV5=00rTuGHC8abGglid/E9QYChiNfcWN+qdtFcHgiGwCcgpdoBrNafwPo7CvRWNC8EEH6fw3+3mi+DhXSwWjFWN3zKIUSWjqEcDCiYv//+TI1exDdcr3Muz=cHnXDj0bvm0YzPm6cyyXiUumwU++Kv2QhqfEhToU7Ht2gtShqPGg25+Li+RWdr7ddac0anh=j+dCMdPoQKniSThAuNOjY5RrDow38G++hFmP3APnAwEmoLlQ4fKvlpyqpg7K30DnQrUiI4rIMjIIjFcC8njwwC7F+rt8zxxWFjeSREB2W8hKuCpNBm5pNqpfFC73bp11EqZhcjr1fTW3Or4PD7Q4xGcDG7eRqH73OrxqiDD=; ssxmod_itna=eqROGIODkDCUK0LWmcYdDQ5GQlqi=GqaaDl=DKxA5D8D6DQeGTrXiOrCOCGO=DU2mNDuWxe2PI8jB0RGIDUGwEQavex0aDbqGkeger4GGIxBYDQxAYDGDDPDocPD1D3qDkD7h6CMy1qGWDm4kDWPDYxDrjOKDRxi7DDHdkx07DQ5koUawcagCepA4jxG1H40HGC8KjPoffI2zoKf448+ODlKUDC91c2HHa4GdXc5z4fwTsertCYxYAinDHCiKo10r+imnD2hrCQGuexRWdA4ogGUDD==; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22c5e4b23d4441faf18514e05c0c6902254%22%2C%22first_id%22%3A%2218964329f23490-0a0783d3e4bfe08-26031d51-1327104-18964329f2460c%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%2218964329f23490-0a0783d3e4bfe08-26031d51-1327104-18964329f2460c%22%7D'
                    }
                    req = request.Request(url=url, headers=header)
                    openhtml = request.urlopen(req).read().decode('utf8')

                    # 正则表达式
                    com = re.compile('"url800":.*?/creative/.*?.jpg"')

                    # 匹配URl地址
                    urladds = com.findall(openhtml)

                    con =re.compile('<.*?class="imgWaper".*?title=.*?" title="(.*?)图片.*?</a>')
                    urlname=con.findall(openhtml)

                    for index,urladd in enumerate(urladds):
                        # try ... except防止匹配出错后程序停止
                        try:
                            add = 'http:' + urladd.strip('"url800":')

                            filename = urlname[index]+'.jpg'
                            path = get_path(keyword[class_index], gender_file_path[gender_index], filename)
                            print('当前下载...', filename)

                            dom = request.urlopen(add).read()
                            with open(path, 'wb') as f:
                                f.write(dom)
                                sum_all_num += 1
                                num_in_page += 1
                        except:
                            print('当前该任务总共总共下载:', sum_all_num)  # 监控进度
                        if sum_all_num % 50 == 0:  # 监控进度
                            print('当前该任务总共下载:', sum_all_num)
