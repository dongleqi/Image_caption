# 导入相关包
import os
import re
import random
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow.keras.applications import VGG19
from tensorflow.keras.applications import VGG16
from keras.layers import Dense, Flatten, Dropout
from keras.layers import Conv2D, DepthwiseConv2D, Dense, GlobalAveragePooling2D
from keras.layers import Activation, BatchNormalization, Add, Lambda
from tensorflow.keras.applications import efficientnet
from tensorflow import keras
from tensorflow.keras import layers


# 导入预训练CNN
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras.applications import VGG19
from keras.applications.inception_v3 import InceptionV3



import json
import jieba
import tqdm

# 设置基本参数

# 图片地址
IMAGES_PATH = 'data\\validation_images'

# 目标大小
IMAGE_SIZE = (299, 299)

# 词汇量大小
VOCAB_SIZE = 10000

# 输出句子单词长度
SEQ_LENGTH = 25

# 特征向量长度
EMBED_DIM = 512

# 输出层维度大小
FF_DIM = 512

# 训练参数
BATCH_SIZE = 32
EPOCHS = 15
AUTOTUNE = tf.data.AUTOTUNE

JSON_PATH = 'data\\data.txt'
FILE_PATH = 'data\\validation_annotations.json'
strip_chars = "!\"#$%&'()*+,-./:;=?@[\]^_`{|}~"


image_augmentation = tf.keras.Sequential(
    [
        tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal"),
        tf.keras.layers.experimental.preprocessing.RandomRotation(0.2),
        tf.keras.layers.experimental.preprocessing.RandomContrast(0.3),
    ]
)