from tkinter import *
import tkinter.filedialog
from PIL import Image, ImageTk
from main import *
class GUI:
    def __init__(self):
        self.path_=None
        self.img_ = None
        self.result_ = None
        self.model = None
        self.root = tkinter.Tk()
        self.root.title("图片显示")
        self.root.geometry("600x500+200+200")
        self.path = StringVar()
        self.button1 = Button(self.root, text='选择图片', command=self.activationLiner)
        self.button1.pack()
        self.entry = Entry(self.root,text=self.path, state='readonly', width=100)
        self.entry.pack()
        self.l1 = Label(self.root)
        self.l1.pack()
        self.root.mainloop()
    def choosePic(self):
        self.path_ = tkinter.filedialog.askopenfilename()
        pass
    def getPic(self):
        self.img_ = Image.open(self.path_)
        pass
    def getResult(self):
        self.result_ = predict_imgs(self.path_)
    def showPic(self):
        img = ImageTk.PhotoImage(self.img_.resize((400, 400)))
        self.l1.config(image=img)
        self.l1.image = img  # keep a reference
        pass
    def showResult(self):
        self.path.set(self.result_)
        pass
    def activationLiner(self):
        self.choosePic()
        self.getPic()
        self.showPic()
        self.getResult()
        self.showResult()
        pass
if __name__ =='__main__':
    GUI()