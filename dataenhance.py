import cv2
from math import *
import os
import numpy as np
import init
pic_num = 0
image_path = init.IMAGES_PATH
size = 300

def remote(img, degree):
    height, width = img.shape[:2]
    radians = float(degree / 180 * pi)  # 1度 = 派（pi）/180 弧度
    heightNew = int(width * fabs(sin(radians)) + height * fabs(cos(radians)))
    widthNew = int(height * fabs(sin(radians)) + width * fabs(cos(radians)))
    # 得到二维矩阵的旋转的仿射矩阵
    matRotation = cv2.getRotationMatrix2D((width / 2, height / 2), degree, 1)
    # 中心位置的实际平移
    matRotation[0, 2] += (widthNew - width) / 2
    matRotation[1, 2] += (heightNew - height) / 2
    imgRotation = cv2.warpAffine(img, matRotation, (widthNew, heightNew), borderValue=(255, 255, 255))
    # 获取原始图像宽高。
    height, width = imgRotation.shape[0], imgRotation.shape[1]
    # 等比例缩放尺度。
    scale = height / size
    # 获得相应等比例的图像宽度。
    width_size = int(width / scale)
    # resize
    image_resize = cv2.resize(imgRotation, (width_size, size))
    return image_resize




def lightshi(img, contrast_ratio, bright):
    rows, cols, channels = img.shape
    blank = np.zeros([rows, cols, channels], img.dtype)
    rst = cv2.addWeighted(img, contrast_ratio, blank, 1 - contrast_ratio, bright)
    return rst



def lightos():
    for dirpath, dirnames, filenames in os.walk(image_path):
        for filename in filenames:
            img = cv2.imread(os.path.join(dirpath, filename))
            bright = 0
            num = 1
            while bright != 200:
                num += 1
                c = 0.8
                bright += 50
                rst = lightshi(img, c, bright)
                cv2.imwrite(dirpath + '/' + filename.split('.')[0] + '-' + "light" + str(num) + '.jpg', rst)


def gaosi(iamg, sigma):
    img_height, img_width, img_channels = iamg.shape
    # 设置高斯（正态分布）分布的均值和方差
    mean = 0
    # 根据均值和标准差生成符合高斯分布的噪声
    gauss = np.random.normal(mean, sigma, (img_height, img_width, img_channels))
    # 给图片添加高斯噪声
    noisy_img = iamg + gauss
    # 设置图片添加高斯噪声之后的像素值的范围
    noisy_img = np.clip(noisy_img, a_min=0, a_max=255)
    # 保存图片
    return noisy_img



def gaosios():
    for dirpath, dirnames, filenames in os.walk(image_path):
        for filename in filenames:
            iamg = cv2.imread(os.path.join(image_path, filename))
            sigma = 0
            while sigma != 20:
                sigma += 40
                img_gaosi = gaosi(iamg, sigma)
                cv2.imwrite(image_path + '/' + filename.split('.')[0] + '-' + "Noise" + str(sigma) + ".jpg", img_gaosi)

if __name__ == '__main__':

    for dirpath, dirnames, filenames in os.walk(image_path):
        for filename in filenames:
            img = cv2.imread(os.path.join(image_path, filename))

            brights = [-50,50]
            num = 1
            agrees = [90,180]
            sigmas = [15,30]

            for agree in agrees:
                imgRotation = remote(img, agree)
                cv2.imwrite(image_path + '/' + filename.split('.')[0] + '_' + str(agree) + ".jpg",
                            imgRotation)
            for bright in brights:
                num += 1
                c = 0.8
                rst = lightshi(img, c, bright)
                cv2.imwrite(dirpath + '/' + filename.split('.')[0] + '_' + "light" + str(num) + '.jpg', rst)

            for sigma in sigmas:
                img_gaosi = gaosi(img, sigma)
                cv2.imwrite(image_path + '/' + filename.split('.')[0] + '_' + "Noise" + str(sigma) + ".jpg", img_gaosi)
    print("end")