from init import *
class loadData():
    def __init__(self):
        self.JSON_PATH = JSON_PATH
        self.FILE_PATH = FILE_PATH
        self.IMAG_PATH = IMAGES_PATH
        self.MAX_LEN = SEQ_LENGTH
        self.caption_mapping = {}  # 映射字典  image1(path):[caption1,caption2...]
        self.text_data = []  # 合格的处理好的caption
        self.images_to_skip = set()  # 不合格的标注
        self.json_data = None

    def read_json(self):
        with open(self.FILE_PATH,encoding='utf-8') as f:
            self.json_data = json.load(f)


    def select(self, tokens, img_path):
        if len(tokens) < 3 or len(tokens) > self.MAX_LEN:
            self.images_to_skip.add(img_path)
            return
        if img_path.endswith("jpg") and img_path not in self.images_to_skip:
            caption = "<start> " +" ".join(tokens).strip()+ " <end>"

            self.text_data.append(caption)
            if img_path in self.caption_mapping:
                self.caption_mapping[img_path].append(caption)
            else:
                self.caption_mapping[img_path] = [caption]

    def cut_words(self):
        for item in tqdm.tqdm(self.json_data):
            img_name = item['image_id']
            img_path = os.path.join(self.IMAG_PATH, img_name.strip())  # 图片的路径
            for caption in item['caption']:  # 遍历属于每个图片的5个标注
                tokens = [word for word in jieba.cut(caption)]  # 分词
                # self.token_len.append(len(tokens))  # 句子长度
                self.select(tokens, img_path)

    def skip_img(self):
        for img_path in self.images_to_skip:
            if img_path in self.caption_mapping:
                del self.caption_mapping[img_path]

    def get_lable(self):
        global f
        try:
            f = open(self.JSON_PATH, 'r')
        except Exception as e:
            f = open(self.JSON_PATH, 'w')  # 没有这个文件，则新建
            self.read_json()
            self.cut_words()
            self.skip_img()

            with open('data\\data.txt', 'w') as f:
                json.dump(self.caption_mapping, f, indent=4, sort_keys=True)
        else:
            data_str = f.read()
            self.caption_mapping = json.loads(data_str)
            self.text_data =[]
            for key in self.caption_mapping:
                self.text_data+=self.caption_mapping[key]
        finally:
            f.close()
        return self.caption_mapping, self.text_data

if __name__ == "__main__":
    cap ,txt = loadData().get_lable()
    for i in list(cap.items())[:5]:
        print(i)
    print(txt[:25])
