import pandas as pd

from init import *
from DB import loadData
from GetModel import get_mobilenet_model,get_cnn_model,get_vgg19_model,get_inceptation_model,get_EFB4_model

GET_MODL = get_EFB4_model()

def custom_standardization(input_string):
    # 全部转为小写
    lowercase = tf.strings.lower(input_string)
    return tf.strings.regex_replace(lowercase, "[%s]" % re.escape(strip_chars), "")


captions_mapping, text_data = loadData().get_lable()
def loaddata():
    train_size = 0.95

    # all_images列表里是所有图片的文件路径
    all_images = list(captions_mapping.keys())
    # 打乱顺序
    np.random.shuffle(all_images)
    # 获取训练集数量
    train_size = int(len(captions_mapping) * train_size)

    train_data = {
        img_name: captions_mapping[img_name] for img_name in all_images[:train_size]
    }
    valid_data = {
        img_name: captions_mapping[img_name] for img_name in all_images[train_size:]
    }
    return train_data, valid_data

train_data, valid_data = loaddata()

vectorization = TextVectorization(
    max_tokens=VOCAB_SIZE,  # 词汇量大小 最上方设置10000
    output_mode="int",
    output_sequence_length=SEQ_LENGTH,  # 输出句子长度 最上方设置25
    standardize=custom_standardization,
)
vectorization.adapt(text_data)


def decode_and_resize(img_path):
    # 读取图片，并缩放

    img = tf.io.read_file(img_path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, IMAGE_SIZE)
    img = tf.image.convert_image_dtype(img, tf.float32)
    return img


def process_input(img_path, captions):
    return decode_and_resize(img_path), vectorization(captions)


def make_dataset(images, captions):
    dataset = tf.data.Dataset.from_tensor_slices((images, captions))
    dataset = dataset.shuffle(len(images))
    dataset = dataset.map(process_input, num_parallel_calls=AUTOTUNE)
    dataset = dataset.batch(BATCH_SIZE).prefetch(AUTOTUNE)

    return dataset


# 制作数据集
train_dataset = make_dataset(list(train_data.keys()), list(train_data.values()))
valid_dataset = make_dataset(list(valid_data.keys()), list(valid_data.values()))

class TransformerEncoderBlock(layers.Layer):
    def __init__(self, embed_dim, dense_dim, num_heads, **kwargs):
        super().__init__()
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.attention_1 = layers.MultiHeadAttention(  # multi head attention
            num_heads=num_heads, key_dim=embed_dim, dropout=0.1
        )  # 头的数量  输出维度的大小  dropout
        self.layernorm_1 = layers.LayerNormalization()
        self.layernorm_2 = layers.LayerNormalization()
        self.dense_1 = layers.Dense(embed_dim, activation="relu")

    def call(self, inputs, training, mask=None):
        # layer norm
        inputs = self.layernorm_1(inputs)
        inputs = self.dense_1(inputs)

        # 传入 q k v
        attention_output_1 = self.attention_1(
            query=inputs,
            value=inputs,
            key=inputs,
            attention_mask=None,
            training=training,  # training：布尔值，表示推理还是训练（是否使用 dropout）
        )
        # residual然后再layer norm
        out_1 = self.layernorm_2(inputs + attention_output_1)  # 残差链接
        return out_1


class PositionalEmbedding(layers.Layer):
    # 位置编码
    def __init__(self, sequence_length, vocab_size, embed_dim, **kwargs):
        super().__init__()
        '''
        embedding用法：https://stats.stackexchange.com/questions/270546/how-does-keras-embedding-layer-work
        input_dim：词汇数量；output_dim：特征向量大小
        '''
        # token embedding：长度为vocab_size，特征向量为：embed_dim
        self.token_embeddings = layers.Embedding(
            input_dim=vocab_size, output_dim=embed_dim
        )
        # position_embeddings：
        self.position_embeddings = layers.Embedding(
            input_dim=sequence_length, output_dim=embed_dim
        )
        self.sequence_length = sequence_length
        self.vocab_size = vocab_size
        self.embed_dim = embed_dim

        # 512开根号：22.627416998：https://jalammar.github.io/illustrated-transformer/
        self.embed_scale = tf.math.sqrt(tf.cast(embed_dim, tf.float32))

    def call(self, inputs):
        # 获取caption长度，这里是24个（前24个单词，去掉<end>）
        length = tf.shape(inputs)[-1]

        # 生成0~length(即24)的数字
        positions = tf.range(start=0, limit=length, delta=1)

        # 输入的句子index转为embedding特征，大小：(N, 24, 512)
        embedded_tokens = self.token_embeddings(inputs)
        # 乘以22.62  上面开根号了 这里乘过去 反向传播好算
        embedded_tokens = embedded_tokens * self.embed_scale

        # 位置编码，大小：(24, 512)
        embedded_positions = self.position_embeddings(positions)

        # 加和 返回
        return embedded_tokens + embedded_positions


class TransformerDecoderBlock(layers.Layer):

    def __init__(self, embed_dim, ff_dim, num_heads, **kwargs):
        super().__init__()
        self.embed_dim = embed_dim
        self.ff_dim = ff_dim
        self.num_heads = num_heads
        self.attention_1 = layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim, dropout=0.1
        )
        self.attention_2 = layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim, dropout=0.1
        )
        self.ffn_layer_1 = layers.Dense(ff_dim, activation="relu")
        self.ffn_layer_2 = layers.Dense(embed_dim)

        self.layernorm_1 = layers.LayerNormalization()
        self.layernorm_2 = layers.LayerNormalization()
        self.layernorm_3 = layers.LayerNormalization()

        # 位置编码
        self.embedding = PositionalEmbedding(
            embed_dim=EMBED_DIM, sequence_length=SEQ_LENGTH, vocab_size=VOCAB_SIZE
        )

        self.out = layers.Dense(VOCAB_SIZE, activation="softmax")

        self.dropout_1 = layers.Dropout(0.1)
        self.dropout_2 = layers.Dropout(0.1)
        self.supports_masking = True

    def call(self, inputs, encoder_outputs, training, mask=None):
        # 获取位置编码，(N,24) --> (N,24,512)
        inputs = self.embedding(inputs)

        '''
        causal_mask 的 shape:(64,24,24)
        64个一模一样，大小为(24, 24)的mask

        '''
        causal_mask = self.get_causal_attention_mask(inputs)

        '''
        mask (64,24) --> padding_mask (64, 24, 1)
        padding_mask：64个大小为(24, 1)的mask
        [[1][1][1]...[0][0][0][0][0]]
        '''
        padding_mask = tf.cast(mask[:, :, tf.newaxis], dtype=tf.int32)

        '''
        mask (64,24) --> combined_mask (64, 1, 24)
        combined_mask：64个大小为(1, 24)的mask
        [[1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]]
        '''
        combined_mask = tf.cast(mask[:, tf.newaxis, :], dtype=tf.int32)

        '''
        在combined_mask与causal_mask选择最小值，大小(64, 24, 24)
        64个不再一模一样，大小为(24, 24)的mask
        '''

        combined_mask = tf.minimum(combined_mask, causal_mask)

        # 第一个masked self  attention，QKV都是inputs, mask是causal mask，强制训练时只关注输出位置左侧的token，以便模型可以自回归地推断

        attention_output_1 = self.attention_1(
            query=inputs,
            value=inputs,
            key=inputs,
            attention_mask=combined_mask,
            training=training,
        )
        out_1 = self.layernorm_1(inputs + attention_output_1)

        # cross attention，其中K、V来自encoder，Q来自decoder前一个的attention输出，mask是padding mask，用来遮挡25个单词中补白的部分
        attention_output_2 = self.attention_2(
            query=out_1,
            value=encoder_outputs,
            key=encoder_outputs,
            attention_mask=padding_mask,
            training=training,
        )
        out_2 = self.layernorm_2(out_1 + attention_output_2)

        ffn_out = self.ffn_layer_1(out_2)
        ffn_out = self.dropout_1(ffn_out, training=training)
        ffn_out = self.ffn_layer_2(ffn_out)

        ffn_out = self.layernorm_3(ffn_out + out_2, training=training)
        ffn_out = self.dropout_2(ffn_out, training=training)

        # 最后输出为VOCAB_SIZE大小的向量，对应位置的大小为概率，可以查索引来获取相应原单词
        preds = self.out(ffn_out)
        return preds

    def get_causal_attention_mask(self, inputs):
        '''
        causal: 因果关系mask
        '''
        # (N,24,512)
        input_shape = tf.shape(inputs)
        # 分别为N，24
        batch_size, sequence_length = input_shape[0], input_shape[1]

        # 范围0~24的列表，变成大小(24, 1)的数组
        i = tf.range(sequence_length)[:, tf.newaxis]
        # 范围0~24的列表
        j = tf.range(sequence_length)
        mask = tf.cast(i >= j, dtype="int32")

        # 大小为(1, 24, 24)
        mask = tf.reshape(mask, (1, input_shape[1], input_shape[1]))

        scale = tf.concat(
            [tf.expand_dims(batch_size, -1), tf.constant([1, 1], dtype=tf.int32)],
            axis=0,
        )
        # (1, 24, 24)铺成(64, 24, 24)
        result = tf.tile(mask, scale)

        return result


class ImageCaptioningModel(keras.Model):
    def __init__(
            self, cnn_model, encoder, decoder, num_captions_per_image=5, image_aug=None,
    ):
        super().__init__()
        self.cnn_model = cnn_model
        self.encoder = encoder
        self.decoder = decoder
        self.loss_tracker = keras.metrics.Mean(name="loss")
        self.acc_tracker = keras.metrics.Mean(name="accuracy")
        self.num_captions_per_image = num_captions_per_image
        self.image_aug = image_aug

    def calculate_loss(self, y_true, y_pred, mask):
        loss = self.loss(y_true, y_pred)
        mask = tf.cast(mask, dtype=loss.dtype)
        loss *= mask
        return tf.reduce_sum(loss) / tf.reduce_sum(mask)

    def calculate_accuracy(self, y_true, y_pred, mask):
        accuracy = tf.equal(y_true, tf.argmax(y_pred, axis=2))
        accuracy = tf.math.logical_and(mask, accuracy)
        accuracy = tf.cast(accuracy, dtype=tf.float32)
        mask = tf.cast(mask, dtype=tf.float32)
        return tf.reduce_sum(accuracy) / tf.reduce_sum(mask)

    def _compute_caption_loss_and_acc(self, img_embed, batch_seq, training=True):
        '''
        计算loss
        '''
        # 图片的embedding特征输入encoder,得到新的seq，大小（N,100,512）
        encoder_out = self.encoder(img_embed, training=training)

        # batch_seq的shape：(64, 25)
        # 前24个单词（去尾）
        batch_seq_inp = batch_seq[:, :-1]

        # 后24个单词（掐头），用做ground truth标注
        batch_seq_true = batch_seq[:, 1:]

        # mask掩码，将batch_seq_true中的每一个元素和0作对比，返回类似[true,true,false]形式的mask，遇到0，则会变成false，0表示字符串中长度不够25的补白部分（padding）
        mask = tf.math.not_equal(batch_seq_true, 0)

        # 输入decoder预测的序列
        batch_seq_pred = self.decoder(
            batch_seq_inp, encoder_out, training=training, mask=mask
        )
        # 计算loss和acc
        loss = self.calculate_loss(batch_seq_true, batch_seq_pred, mask)
        acc = self.calculate_accuracy(batch_seq_true, batch_seq_pred, mask)
        return loss, acc

    def train_step(self, batch_data):
        '''
        训练步骤
        '''
        # 获取图片和标注
        batch_img, batch_seq = batch_data
        # 初始化
        batch_loss = 0
        batch_acc = 0
        # 是否使用数据增强
        if self.image_aug:
            batch_img = self.image_aug(batch_img)

        # 获取图片embedding特征
        img_embed = self.cnn_model(batch_img)

        # 遍历5个文本标注
        for i in range(self.num_captions_per_image):
            with tf.GradientTape() as tape:
                # 计算loss和acc
                # batch_seq的shape：(64, 5, 25)
                loss, acc = self._compute_caption_loss_and_acc(
                    img_embed, batch_seq[:, i, :], training=True
                )

                # 更新loss和acc
                batch_loss += loss
                batch_acc += acc

            # 获取所有可训练参数
            train_vars = (
                    self.encoder.trainable_variables + self.decoder.trainable_variables
            )

            # 获取梯度
            grads = tape.gradient(loss, train_vars)

            # 更新参数
            self.optimizer.apply_gradients(zip(grads, train_vars))

        # 更新
        batch_acc /= float(self.num_captions_per_image)
        self.loss_tracker.update_state(batch_loss)
        self.acc_tracker.update_state(batch_acc)

        return {"loss": self.loss_tracker.result(), "acc": self.acc_tracker.result()}

    def test_step(self, batch_data):
        batch_img, batch_seq = batch_data
        batch_loss = 0
        batch_acc = 0

        # 获取图片embedding特征
        img_embed = self.cnn_model(batch_img)

        # 遍历5个文本标注
        for i in range(self.num_captions_per_image):
            loss, acc = self._compute_caption_loss_and_acc(
                img_embed, batch_seq[:, i, :], training=False
            )

            batch_loss += loss
            batch_acc += acc

        batch_acc /= float(self.num_captions_per_image)

        self.loss_tracker.update_state(batch_loss)
        self.acc_tracker.update_state(batch_acc)

        return {"loss": self.loss_tracker.result(), "acc": self.acc_tracker.result()}

    @property
    def metrics(self):
        return [self.loss_tracker, self.acc_tracker]


# loss
cross_entropy = keras.losses.SparseCategoricalCrossentropy(
    from_logits=False, reduction="none"
)

# 提前终止
early_stopping = keras.callbacks.EarlyStopping(patience=3, restore_best_weights=True)


class LRSchedule(keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, post_warmup_learning_rate, warmup_steps):
        super().__init__()
        self.post_warmup_learning_rate = post_warmup_learning_rate
        self.warmup_steps = warmup_steps

    def __call__(self, step):
        global_step = tf.cast(step, tf.float32)
        warmup_steps = tf.cast(self.warmup_steps, tf.float32)
        warmup_progress = global_step / warmup_steps
        warmup_learning_rate = self.post_warmup_learning_rate * warmup_progress
        return tf.cond(
            global_step < warmup_steps,
            lambda: warmup_learning_rate,
            lambda: self.post_warmup_learning_rate,)


cnn_model = GET_MODL
encoder = TransformerEncoderBlock(embed_dim=EMBED_DIM, dense_dim=FF_DIM, num_heads=4)
decoder = TransformerDecoderBlock(embed_dim=EMBED_DIM, ff_dim=FF_DIM, num_heads=8)
caption_model = ImageCaptioningModel(
    cnn_model=cnn_model, encoder=encoder, decoder=decoder, image_aug=image_augmentation,
)
# LR调节
if not os.path.exists('./my_model/checkpoint'):
    num_train_steps = len(train_dataset) * EPOCHS
    num_warmup_steps = num_train_steps // 15
    lr_schedule = LRSchedule(post_warmup_learning_rate=1e-4, warmup_steps=num_warmup_steps)

    # 编译
    caption_model.compile(optimizer=keras.optimizers.Adam(lr_schedule), loss=cross_entropy)

    # 训练

    history = caption_model.fit(
        train_dataset,
        epochs=EPOCHS,
        validation_data=valid_dataset,
        # callbacks=[early_stopping],
    )

    caption_model.save_weights("./my_model/checkpoint")

    print(history.history)
    import pandas as pd

    pd.DataFrame(history.history, index=range(3)).to_csv('col_count.csv',index=False,encoding='gbk')


load_status = caption_model.load_weights("./my_model/checkpoint")

vocab = vectorization.get_vocabulary()
index_lookup = dict(zip(range(len(vocab)), vocab))
max_decoded_sentence_length = SEQ_LENGTH - 1
valid_images = list(valid_data.keys())
valid_caption = list(valid_data.values())
valid_len = len(valid_images)


def generate_caption():
    # 在测试集中随机取一张图片
    random_index = random.randrange(0, valid_len)
    sample_img = valid_images[random_index]
    sample_caption = valid_caption[random_index][0]
    # 读取图片
    sample_img = decode_and_resize(sample_img)
    # img_show = sample_img.numpy().clip(0, 255).astype(np.uint8)
    #
    # plt.imshow(img_show)
    # plt.axis('off')
    # plt.show()

    # 保存
    # cv2.imwrite('./img/raw.jpg', cv2.cvtColor(img_show, cv2.COLOR_RGB2BGR))
    # 获取CNN特征
    img = tf.expand_dims(sample_img, 0)
    img = caption_model.cnn_model(img)

    # 传给encoder
    encoded_img = caption_model.encoder(img, training=False)

    # 1.先提供"<start> "
    # 2.传给decoder推理，
    # 3.不断投喂给模型，直到遇到<end>停止
    # 4.如果循环次数超出句子长度，也停止
    decoded_caption = "<start> "
    for i in range(max_decoded_sentence_length):  # 24
        tokenized_caption = vectorization([decoded_caption])[:, :-1]
        mask = tf.math.not_equal(tokenized_caption, 0)

        # 预测
        predictions = caption_model.decoder(
            tokenized_caption, encoded_img, training=False, mask=mask
        )
        sampled_token_index = np.argmax(predictions[0, i, :])
        sampled_token = index_lookup[sampled_token_index]
        if sampled_token == " <end>":
            break
        decoded_caption += " " + sampled_token

    decoded_caption = decoded_caption.replace("<start> ", "")
    decoded_caption = decoded_caption.replace(" <end>", "").strip()

    sample_caption = sample_caption.replace("<start> ", "")
    sample_caption = sample_caption.replace(" <end>", "").strip()

    print("预测: ", decoded_caption)
    print('真实：', sample_caption)


# generate_caption()


def predict_imgs(path):
    input_img = decode_and_resize(path).numpy().clip(0, 255).astype(np.uint8)
    # 获取CNN特征
    img = tf.expand_dims(input_img, 0)
    img = caption_model.cnn_model(img)
    # 传给encoder
    encoded_img = caption_model.encoder(img, training=False)

    # 1.先提供"<start> "
    # 2.传给decoder推理，
    # 3.不断投喂给模型，直到遇到<end>停止
    # 4.如果循环次数超出句子长度，也停止
    decoded_caption = "<start> "
    for i in range(max_decoded_sentence_length):  # 24
        tokenized_caption = vectorization([decoded_caption])[:, :-1]
        mask = tf.math.not_equal(tokenized_caption, 0)

        # 预测
        predictions = caption_model.decoder(
            tokenized_caption, encoded_img, training=False, mask=mask
        )
        sampled_token_index = np.argmax(predictions[0, i, :])
        sampled_token = index_lookup[sampled_token_index]
        if sampled_token == "<end>":
            break
        decoded_caption += " " + sampled_token
    decoded_caption = decoded_caption.replace("<start> ", "")
    decoded_caption = decoded_caption.replace(" <end>", "").strip()

    return "预测: " + decoded_caption